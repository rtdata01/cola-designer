const env = {
    active: 'dev',//preview,dev
    baseUrl: '/design',
    fileUrl: '/file',
    version: '1.8.8',
}
export const baseUrl = env.baseUrl
export const fileUrl = env.fileUrl
export default env
